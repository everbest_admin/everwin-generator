**项目说明** 
- everwin-generator是基于人人开源项目的代码生成器，可在线生成entity、xml、dao、service、html、js、sql代码，减少70%以上的开发任务
<br> 

- !!! 注意 :  <br/>
1.每次进行代码生成前需要修改 application.yml 中对应的数据库配置信息  <br/>
2.generator.properties 中需要正确配置 包名 和 路径名称    <br/>
3.逻辑删除字段需添加   @TableLogic (局部配置)字段并在 配置文件中进行配置(全局配置) <br/>
4.自动填充字段需要添加  @TableField(fill = FieldFill.INSERT) 并实现 MetaObjectHandler 进行配置
5.数值/时间等类型需要 序列化(转字符串)  @JsonSerialize(using = ToStringSerializer.class) ,否则会导致精度丢失 
(Long 类型雪花ID可达19位,js Number 类型最长16位,会造成精度丢失; 时间类型Date ,LocalDate<Time> 也是)
               
  
 **本地部署**
- 通过git下载源码
- 修改application.yml，更新MySQL账号和密码、数据库名称
- Eclipse、IDEA运行everwinApplication.java，则可启动项目
- 项目访问路径：http://localhost:8090
